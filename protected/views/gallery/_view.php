<?php
/* @var $this GalleryController */
/* @var $data Gallery */
?>

<div class="galery-item">
	<div class="galery-title"><?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?></div>
	<div class="galery-img" style="background-image: url(/upload/<?=CHtml::encode($data->image)?>)"></div>
</div>
