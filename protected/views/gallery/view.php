<?php
/* @var $this GalleryController */
/* @var $model Gallery */

$this->breadcrumbs=array(
	'Galleries'=>array('index'),
	$model->title,
);
?>

<h1>View Gallery #<?php echo $model->id; ?></h1>

<h2><?= $model->title ?></h2>

<div class="galery-img">
	<img src="/upload/<?= $model->image?>" alt="<?= $model->alt ?>">
</div>
