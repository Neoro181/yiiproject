<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-16">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-8 last">
	<div id="sidebar">
	<?php
		// $this->beginWidget('zii.widgets.CPortlet', array(
		// 	'title'=>'Operations',
		// ));
		// $this->widget('zii.widgets.CMenu', array(
		// 	'items'=>$this->menu,
		// 	'htmlOptions'=>array('class'=>'operations'),
		// ));
		// $this->endWidget();
		$this->widget('zii.widgets.CMenu',array(
      'items'=>$this->topMenu
    ));
		// var_dump($this);
	?>
	</div><!-- sidebar -->
	<div class="gallery-widget">
		<?php $this->widget('application.widget.GalleryWidget.GalleryWidget'); ?>
	</div>
</div>
<?php $this->endContent(); ?>
