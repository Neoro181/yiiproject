<?php
class AccessCommand extends CConsoleCommand {
    public function actionAddRules() {
      $auth=Yii::app()->authManager;
      $auth->createOperation('login',Yii::t("main", "Login"));
      $auth->createOperation('indexAuthor',Yii::t("main", "View list of authors"));
      $auth->createOperation('createAuthor',Yii::t("main", "Create author"));
      $auth->createOperation('adminAuthor',Yii::t("main", "Administrate authors"));
      $auth->createOperation('updateAuthor',Yii::t("main", "Update author"));
      $auth->createOperation('deleteAuthor',Yii::t("main", "Delete author"));
      $auth->createOperation('makePageAuthor',Yii::t("main", "Make site's page for that author"));

      $auth->createOperation('indexBook',Yii::t("main", "View list of books"));
      $auth->createOperation('createBook',Yii::t("main", "Create book"));
      $auth->createOperation('adminBook',Yii::t("main", "Administrate books"));
      $auth->createOperation('updateBook',Yii::t("main", "Update book"));
      $auth->createOperation('deleteBook',Yii::t("main", "Delete book"));
      $auth->createOperation('selectImageBook',Yii::t("main", "Make list of book's images"));
      $auth->createOperation('getAuthorsBook',Yii::t("main", "Get authors of book"));

      $auth->createOperation('indexAuthorBook',Yii::t("main", "View list of links authors and books"));
      $auth->createOperation('createAuthorBook',Yii::t("main", "Create link author and book"));
      $auth->createOperation('adminAuthorBook',Yii::t("main", "Administrate link author and books"));
      $auth->createOperation('updateAuthorBook',Yii::t("main", "Update link author and book"));
      $auth->createOperation('deleteAuthorBook',Yii::t("main", "Delete link author and book"));

      $role=$auth->createRole('guest');
      $role->addChild('login');

      $role=$auth->createRole('user');
      $role->addChild('guest');

      $role=$auth->createRole('manager');
      $role->addChild('user');
      $role->addChild('indexAuthor');
      $role->addChild('createAuthor');
      $role->addChild('adminAuthor');
      $role->addChild('updateAuthor');
      $role->addChild('deleteAuthor');
      $role->addChild('makePageAuthor');
      $role->addChild('indexBook');
      $role->addChild('createBook');
      $role->addChild('adminBook');
      $role->addChild('updateBook');
      $role->addChild('deleteBook');
      $role->addChild('selectImageBook');
      $role->addChild('getAuthorsBook');
      $role->addChild('indexAuthorBook');
      $role->addChild('createAuthorBook');
      $role->addChild('adminAuthorBook');
      $role->addChild('updateAuthorBook');
      $role->addChild('deleteAuthorBook');
      $role=$auth->createRole('administrator');
      $role->addChild('manager');
    }

    public function actionAssignUsers() {
      Yii::app()->authManager->assign('administrator', '2');
      Yii::app()->authManager->assign('manager', '3');
      Yii::app()->authManager->assign('manager', '4');
    }
}
