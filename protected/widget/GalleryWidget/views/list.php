<ul class="gallery-widget-list">
  <?php foreach($gallery as $item): ?>
	  <li class="gallery-widget-item">

			<div class="gallery-widget-title">
				<?php echo CHtml::link(CHtml::encode($item['title']), array('/gallery/view', 'id'=>$item->id)); ?>
			</div>
			<div class="gallery-widget-image">
				<img src="/upload/<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
			</div>

	  </li>
  <?php endforeach; ?>
</ul>
