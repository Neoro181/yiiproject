<?php
class GalleryWidget extends CWidget
{
    public function run()
    {

      $criteria = new CDbCriteria();
      $criteria->limit = 2;

      $gallery = Gallery::model()->findAll($criteria);

      $this->render('list', compact('gallery'));
    }
}
