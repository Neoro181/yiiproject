<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $user_role_id
 *
 * The followings are the available model relations:
 * @property UserRole $userRole
 */
class User extends CActiveRecord
{
	const DEFAULT_ROLE = '3';
	public $password2 = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, user_role_id', 'required'),
			array('user_role_id', 'numerical', 'integerOnly'=>true),
			array('username, password, email', 'length', 'max'=>255),
			array('email', 'email'),
			array('username, email', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, email, user_role_id', 'safe', 'on'=>'search'),
			array('password2', 'compare', 'compareAttribute' => 'password', 'on' => 'signUp'),
			array('password2', 'required', 'on' => 'signUp'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userRole' => array(self::BELONGS_TO, 'UserRole', 'user_role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'user_role_id' => 'User Role',
			'password2' => 'Repeat password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('user_role_id',$this->user_role_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validatePassword($password) {
    return CPasswordHelper::verifyPassword($password,$this->password);
  }

  public function hashPassword($password) {
    return CPasswordHelper::hashPassword($password);
  }

	public function beforeValidate() {
    if ($this->isNewRecord) {
       $this->date = time();
       $this->user_role_id = self::DEFAULT_ROLE;
    }

    return parent::beforeValidate();
  }

  protected function beforeSave() {
    $this->password = $this->hashPassword($this->password);
    return parent::beforeSave();
  }
}
