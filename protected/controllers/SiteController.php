<?php

class SiteController extends Controller
{

	public $layout='//layouts/column2';
	public $topMenu;
	public $pageTitle;
	public $pageContent;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	 public function actionIndex()
 	{
		$url = 'index';
 		$model = Page::model()->find('url = :url', array(':url'=>$url));

 		if (!$model) throw new CHttpException(404);

 		$this->pageContent = $model->content;
 		$this->pageTitle = $model->title;

 		$this->render('index',array(
 				'model'=>$model,
 		));
 	}



	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionPage($url)
	{
		$model = Page::model()->find('url = :url', array(':url'=>$url));

		if (!$model) throw new CHttpException(404);

		$this->pageContent = $model->content;
		$this->pageTitle = $model->title;

		$this->render('page',array(
				'model'=>$model,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionSignUp() {

		if (!Yii::app()->user->isGuest)
				throw new CHttpException(404, 'Error 404');

		$model = new User('signUp');

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'sign-up-form') {
				echo CActiveForm::validate($model);
				Yii::app()->end();
		}

		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];

			if ($model->save()) {
				$login = new UserIdentity($model->email, $model->password2);
				if ($login->authenticate() == '') {
					Yii::app()->user->login($login, 0);

					Yii::app()->user->setFlash('signUp', 'Registration successful');
					$this->redirect(array('site/index'));
				}
			}
		}

		$this->render('signUp', array('model' => $model));
	}

	protected function beforeAction($action)
   {
		 $this->topMenu = array();
		 $items = Page::model()->findAll();
		 foreach ($items AS $item) {
			 array_push($this->topMenu, array('label'=>$item->title, 'url'=>array('/site/page/'.$item->url)));
		 }

       return parent::beforeAction($action);
   }
}
